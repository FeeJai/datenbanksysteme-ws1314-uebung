class Kandidatur < ActiveRecord::Base
  belongs_to :wahl
  belongs_to :kandidat
  belongs_to :partei, :class_name => 'partei_landesliste'
  belongs_to :partei, :class_name => 'partei_direktkandidatur'
  belongs_to :partei, :class_name => 'partei_mitgliedschaft'
end
