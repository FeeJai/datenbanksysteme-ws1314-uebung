class Stimmrecht < ActiveRecord::Base
  belongs_to :wahllokal
end

=begin

class Stimmrecht < ActiveRecord::Base
  belongs_to :wahllokal

  attr_accessor :password

  before_save :encrypt_password

  def self.authenticate(password, wahllokal)
    stimmrecht = find(:conditions=>["password=? and project_id=?", params[:user_id], params[:project_id]])

    if wahllokal && wahllokal.password_hash == BCrypt::Engine.hash_secret(password, wahllokal.password_salt)
      wahllokal
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
    	self.password_salt = BCrypt::Engine.generate_salt
    	self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

end

=end
