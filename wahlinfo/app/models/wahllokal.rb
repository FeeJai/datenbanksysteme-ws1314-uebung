class Wahllokal < ActiveRecord::Base
  belongs_to :wahlkreis

  attr_accessor :password

  before_save :encrypt_password
  after_save :clear_password


  validates_confirmation_of :password
  validates_presence_of :password, :on => :create

#   attr_accessible :wahlkreis, :wahlleiter, :adresse, :password, :password_confirmation


  def self.authenticate(id, password)
    wahllokal = find_by_id(id)
    if wahllokal && wahllokal.password_hash == BCrypt::Engine.hash_secret(password, wahllokal.password_salt)
      wahllokal
    else
      nil
    end
  end
  

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end


  def clear_password
    self.password = nil
  end

end
