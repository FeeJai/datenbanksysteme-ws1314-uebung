class Wahl < ActiveRecord::Base
  has_many :kandidaturs, dependent: :destroy
  has_many :stimmzettels, dependent: :destroy
end
