class Land < ActiveRecord::Base
  has_many :wahlkreises, dependent: :destroy
  has_many :wahlkreises, dependent: :restrict
end
