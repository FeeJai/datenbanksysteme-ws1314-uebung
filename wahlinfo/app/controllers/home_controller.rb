class HomeController < ApplicationController
  def index

    if params[:jahr]
      @jahr = params[:jahr]
    else
      @jahr = 2013
    end
     

     #{ActiveRecord::Base.sanitize(wk_id)} 

    @Verteilung = Oberverteilung.where("wahl_id = #{ActiveRecord::Base.sanitize(@jahr)}")
  	ober_name = @Verteilung.collect(&:name)
  	ober_sitze = @Verteilung.collect(&:sitze)

    colors = {'SPD' => '#A00010', 'CDU' => '#000000', 'CSU' => '#333333', 'GRÜNE' => '#228B22', 
      'DIE LINKE' => '#D15FEE', 'FDP' => '#ffd700'}

    array = [] 

    ober_name.each_with_index do |name, index| 
      hash = {:name => ober_name[index], :color => colors[name] }
      array.push(hash)    
    end

    ober_sitze.each_with_index do |sitze, index|
      array[index][:y] = sitze.to_f
    end

  	@chart = LazyHighCharts::HighChart.new('pie') do |f|
      f.chart({:defaultSeriesType=>"pie" , :margin=> [50, 200, 60, 170] ,
      	  :backgroundColor=>"#f0f0f0" })
      series = {
               :type=> 'pie',
               :name=> 'Sitzverteilung',
               :data=> array                                        
      }
      f.series(series)
      #f.options[:title][:text] = "Sitzverteilung"
      f.legend(:layout=> 'vertical',:style=> {:left=> 'auto', :bottom=> 'auto',:right=> '50px',:top=> '100px'}) 
      f.plot_options(:pie=>{
        :allowPointSelect=>true, 
        :cursor=>"pointer" , 
        :dataLabels=>{
          :enabled=>true,
          :color=>"black",
          :style=>{
            :font=>"13px Trebuchet MS, Verdana, sans-serif"
          }
        }
      })
    end
  end
end
