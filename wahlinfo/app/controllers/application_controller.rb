class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :logged_in
  helper_method :manager

	private

	def logged_in
	  @logged_in ||= Wahllokal.find(session[:wahllokal_id]) if session[:wahllokal_id]
	end

	def manager
		if session[:manager]
			@manager = true
		else
			@manager = false
		end
	end


end
