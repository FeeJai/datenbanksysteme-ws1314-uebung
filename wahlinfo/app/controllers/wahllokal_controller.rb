class WahllokalController < ApplicationController

	def stimmrecht
		if not manager
			redirect_to root_url, :notice => "Login als Wahlvorstand notwendig!"
		end

		# Formular wurde über Post-Knopf aufgerufen
	  	if params[:commit]
			@wahllokal = Wahllokal.find(session[:wahllokal_id])

			@stimmrecht = Stimmrecht.new
			@stimmrecht.wahllokal = @wahllokal
			@stimmrecht.used = false

	  		password = ''
			chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
			7.times { password << chars[rand(chars.size)] }

			@stimmrecht.password = password
		  	@stimmrecht.save
	  	end

	end

	def new
	  #Signup Form
	  @wahllokal = Wahllokal.new     
	end

	def create
	@wahllokal = Wahllokal.new(user_params)
	if @wahllokal.save
	    flash[:notice] = "Wahllokal erfolgreich angelegt. ID-Nummer ist: " + @wahllokal.id.to_s
	    flash[:color]= "valid"
	    #redirect_to root_url, :notice => "Wahllokal angelegt!
	  	@wahllokal = Wahllokal.new     
	    render "new"
	else
	    flash[:notice] = "Form is invalid"
	    flash[:color]= "invalid"
	    render "new"
	end
	end

	def user_params
	params.require(:wahllokal).permit(:wahlkreis_id, :wahlleiter, :adresse, :password, :password_confirmation, :salt, :encrypted_password)
	end


end
