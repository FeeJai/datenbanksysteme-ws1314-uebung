class SessionsController < ApplicationController
	def new
	end

	def create
	  wahllokal = Wahllokal.authenticate(params[:id], params[:password])
	  if wahllokal
	    session[:wahllokal_id] = wahllokal.id
	    if params[:manager] == "true"
	    	session[:manager] = true
	    else
	    	session[:manager] = false
	  	end
	    redirect_to root_url, :notice => "Logged in!"
	  else
	    #flash.now.alert = "Invalid email or password"
	    flash[:notice] = "Login fehlgeschlagen. Bitte Passwort und Wahllokal-ID prüfen"
	    render "new"
	  end
	end

	def destroy
	  session[:wahllokal_id] = nil
	  session[:manager] = false
	  redirect_to root_url, :notice => "Logged out!"
	end
end

