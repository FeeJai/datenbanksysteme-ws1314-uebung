class VoteController < ApplicationController
  def index

  	if not logged_in
			redirect_to root_url, :notice => "Login notwendig!"
	end

  	if manager
			redirect_to root_url, :notice => "Login notwendig!"
	end


	wahllokal = Wahllokal.find(session[:wahllokal_id])
	@wahlkreis = wahllokal.wahlkreis

	#@erst_kandidaturs  = Kandidatur.where("wahl_id = 2013 AND wahlkreis_id = #{@wahlkreis.id}" )


	sql =  "SELECT kandidats.id, kandidats.name, parteis.name
			FROM kandidaturs
			JOIN kandidats ON kandidaturs.kandidat_id = kandidats.id
			LEFT JOIN parteis ON kandidaturs.partei_id = parteis.id

			WHERE wahl_id = 2013
			AND wahlkreis_id = #{@wahlkreis.id}


			ORDER BY partei_id"

	 result = ActiveRecord::Base.connection.execute(sql)

	 result.values.each do |row|

	 	(@erst_kandidaturs ||= []) <<  row

	 end


	sql =  "SELECT parteis.id, name FROM parteis
			JOIN kandidaturs ON parteis.id = kandidaturs.partei_id
			WHERE listenplatz = 1
			AND wahl_id = 2013
			AND land_id =  #{@wahlkreis.land_id}
			ORDER BY parteis.id"

	 result = ActiveRecord::Base.connection.execute(sql)

	 result.values.each do |row|

	 	(@zweit_kandidaturs ||= []) <<  row

	 end

  end

  def create

  	if not logged_in
		redirect_to root_url, :notice => "Login notwendig!"
	end

  	if manager
			redirect_to root_url, :notice => "Login notwendig!"
	end


	wahllokal = Wahllokal.find(session[:wahllokal_id])

	vote = params[:vote]

	stimmrecht = Stimmrecht.where("password = #{ActiveRecord::Base.sanitize(vote[:password])} AND wahllokal_id=#{session[:wahllokal_id]}")

	if stimmrecht.empty?
		    	flash[:notice] = 'Passwort falsch!' 
				redirect_to action: "index", status: 302
				#raise "passwort falsch"
	else
		stimmrecht = stimmrecht.take

		if stimmrecht.used
			    	flash[:notice] = 'Stimme wurde bereits gezählt!' 
					redirect_to action: "index"
		end


	    stimmrecht.used = true

		erststimme = Erststimme.new(wahlkreis: wahllokal.wahlkreis, kandidat_id: params[:erststimme], wahl_id: "2013")
		zweitstimme = Zweitstimme.new(wahlkreis: wahllokal.wahlkreis, partei_id: params[:zweitstimme], wahl_id: "2013")

		ActiveRecord::Base.transaction do
			erststimme.save!
			zweitstimme.save!
			stimmrecht.save!
		end
	end

	end

end
