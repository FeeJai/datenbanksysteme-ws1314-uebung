class WkuebersichtController < ApplicationController
  def index


  	 


  	#Q3.1 und Q3.2 über View WKInfo

  	if params[:wk]
		wk_id = params[:wk]
	else
		@info = Wkinfo.first
		wk_id = 1
	end

	@info13 = Wkinfo.find_by(id: wk_id, wahl_id: 2013)
	@info09 = Wkinfo.find_by(id: wk_id, wahl_id: 2009)

	@partei13 = Partei.find(@info13.partei_id)
	@partei09 = Partei.find(@info09.partei_id)

	@wahlkreis = Wahlkreis.find(wk_id)


	#Q3.3


	sql = 	"WITH Zweitstimmen AS
				(SELECT partei_id, stimmen, wahl_id
				FROM ViewZweitstimmenAggWk
				WHERE wahlkreis_id = #{ActiveRecord::Base.sanitize(wk_id)}),

			Gesamtstimmen AS
				(SELECT SUM(stimmen) as gesamt, wahl_id
				FROM Zweitstimmen
				GROUP BY wahl_id)

			SELECT partei_id, stimmen as stimmen_absolut, (stimmen/gesamtstimmen.gesamt) as stimmen_relativ, Gesamtstimmen.wahl_id
			FROM Zweitstimmen, Gesamtstimmen
			WHERE Zweitstimmen.wahl_id = Gesamtstimmen.wahl_id
			ORDER BY wahl_id DESC, stimmen_relativ DESC"
			

	 result = ActiveRecord::Base.connection.execute(sql)

	 @wahlergebnis = Hash.new

	 result.values.each do |row|

	 	parteiId = row[0]

	 	if @wahlergebnis.has_key?(parteiId)
	 		zeile = @wahlergebnis[parteiId]
	 	else
	 		zeile = Hash.new
	 	end

	 	if parteiId
	 		zeile[:partei] = Partei.find(parteiId).name
	 	else
	 		zeile[:partei] = 'Ungültige'
	 	end

	 	if 	'2009' == row[3]
	 		zeile[:stimmenabs2009] = row[1]
	 		zeile[:stimmenrel2009] = row[2]
	 	elsif '2013' == row[3]
	 		zeile[:stimmenabs2013] = row[1]
	 		zeile[:stimmenrel2013] = row[2]
	 	end

		@wahlergebnis[parteiId] = zeile

	 	#if row[0]
	 	#	row[0] = Partei.find(row[0]).name
	 	#else
	 	#	row[0] = "Ungültige"
	 	#end
		#(@wahlergebnis ||= []) <<  row

	 end

	 array = []
	 @wahlergebnis.each_value do |ergebnis|
	 		hash = {:type => 'column', :name => ergebnis[:partei], 
	 			:data => [(ergebnis[:stimmenrel2009].to_f * 100).round(2), (ergebnis[:stimmenrel2013].to_f * 100).round(2)]}
	 		array.push(hash)	
    end   

    name = []
    @wahlergebnis.each_value do |ergebnis|
	 		name.push(ergebnis[:partei])	
    end  

    a9 = []
    a13 = []

    @wahlergebnis.each_value do |ergebnis|
	 		a9.push((ergebnis[:stimmenrel2009].to_f * 100).round(2))
	 		a13.push((ergebnis[:stimmenrel2013].to_f * 100).round(2)) 	
    end     

    
    neun = a9.take(5)
    dreiz = a13.take(5)

    

=begin
	 	ergebnis = Array.new
	 	ergebnis.push = Partei.find(@row.partei_id)
=end
@chart = LazyHighCharts::HighChart.new('graph') do |f|
	f.chart(:backgroundColor=>"#f0f0f0")
    # f.title({ :text=>@info13.id})
    f.options[:xAxis][:categories] = name
    #f.labels(:items=>[:html=>"Total fruit consumption", :style=>{:left=>"40px", :top=>"8px", :color=>"black"} ])      
    #array.each{
    #	|x| f.series(x)
    #}
        f.series(:type=> 'column',:name=> '2009',:data=> neun, :color=>'gray')
        f.series(:type=> 'column',:name=> '2013',:data=> dreiz, :color=> 'black')


    f.legend(:layout=> 'horizontal',:style=> {:left=> 'auto', :bottom=> 'auto',:right=> '50px',:top=> '100px'}) 

      
    

  end

	

  end
end
