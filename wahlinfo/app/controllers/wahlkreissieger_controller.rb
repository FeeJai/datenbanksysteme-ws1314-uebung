class WahlkreissiegerController < ApplicationController
  def index


  	if params[:jahr]
		@wahl = params[:jahr]
	else
		@wahl = 2013
	end

  	sql = "WITH MaxErststimmenAggWk(wahlkreis_id, stimmen) AS
	(SELECT wahlkreis_id, MAX(stimmen) as stimmen
	FROM ViewErststimmenAggWk
	WHERE wahl_id = #{ActiveRecord::Base.sanitize(@wahl)}
	GROUP BY wahlkreis_id
	),
	
WkGewinner AS 
	(SELECT v.wahlkreis_id, v.kandidat_id, k.partei_id
	FROM ViewErststimmenAggWk v
	JOIN MaxErststimmenAggWk max
		ON v.stimmen = max.stimmen AND v.wahlkreis_id = max.wahlkreis_id
	JOIN kandidaturs k
		ON  v.kandidat_id = k.kandidat_id
	WHERE  v.wahl_id = k.wahl_id),
	
MaxZweitstimmenAggWk(wahlkreis_id, stimmen) AS
	(SELECT wahlkreis_id, MAX(stimmen) as stimmen
	FROM ViewZweitstimmenAggWk
	GROUP BY wahlkreis_id
	),
	
ParteiMaxZweitstimmen AS 
	(SELECT v.wahlkreis_id, v.partei_id
	FROM ViewZweitstimmenAggWk v
	JOIN MaxZweitstimmenAggWk max
		ON v.stimmen = max.stimmen AND v.wahlkreis_id = max.wahlkreis_id
	)
	
SELECT erst.wahlkreis_id, kandidat_id as Direktkandidat, erst.partei_id as ParteiDirektkandiat, zweit.partei_id as ParteiMeisteZweitstimmen
FROM WkGewinner erst
JOIN ParteiMaxZweitstimmen zweit
ON erst.wahlkreis_id = zweit.wahlkreis_id
ORDER BY erst.wahlkreis_id"


	 result = ActiveRecord::Base.connection.execute(sql)

	 result.values.each do |row|

	 	wk = Array.new
	 	#if row[:partei_id]
	 	#end

	 	wk.push(row[0])
	 	wk.push(Partei.find(row[2]).name)
	 	wk.push(Partei.find(row[3]).name)

		(@wahlergebnis ||= []) <<  wk

	 end



  end
end
