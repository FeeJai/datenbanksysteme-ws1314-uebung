--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: kandidats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE kandidats (
    id integer NOT NULL,
    name character varying(255),
    adresse text,
    geburtsjahr integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: kandidats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE kandidats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: kandidats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE kandidats_id_seq OWNED BY kandidats.id;


--
-- Name: kandidaturs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE kandidaturs (
    id integer NOT NULL,
    wahl_id integer,
    kandidat_id integer,
    listenplatz integer,
    partei_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: kandidaturs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE kandidaturs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: kandidaturs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE kandidaturs_id_seq OWNED BY kandidaturs.id;


--
-- Name: lands; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lands (
    id integer NOT NULL,
    einwohner integer,
    name character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: lands_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lands_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lands_id_seq OWNED BY lands.id;


--
-- Name: parteis; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE parteis (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: parteis_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE parteis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parteis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE parteis_id_seq OWNED BY parteis.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: stimmzettels; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stimmzettels (
    id integer NOT NULL,
    wahlkreis_id integer,
    kandidat_id integer,
    partei_id integer,
    wahl_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: stimmzettels_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stimmzettels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stimmzettels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stimmzettels_id_seq OWNED BY stimmzettels.id;


--
-- Name: wahlkreis; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE wahlkreis (
    id integer NOT NULL,
    name character varying(255),
    wahlberechtigte integer,
    land_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: wahlkreis_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE wahlkreis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wahlkreis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE wahlkreis_id_seq OWNED BY wahlkreis.id;


--
-- Name: wahls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE wahls (
    id integer NOT NULL,
    jahr integer,
    sitze integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: wahls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE wahls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wahls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE wahls_id_seq OWNED BY wahls.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY kandidats ALTER COLUMN id SET DEFAULT nextval('kandidats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY kandidaturs ALTER COLUMN id SET DEFAULT nextval('kandidaturs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lands ALTER COLUMN id SET DEFAULT nextval('lands_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY parteis ALTER COLUMN id SET DEFAULT nextval('parteis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stimmzettels ALTER COLUMN id SET DEFAULT nextval('stimmzettels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY wahlkreis ALTER COLUMN id SET DEFAULT nextval('wahlkreis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY wahls ALTER COLUMN id SET DEFAULT nextval('wahls_id_seq'::regclass);


--
-- Name: kandidats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY kandidats
    ADD CONSTRAINT kandidats_pkey PRIMARY KEY (id);


--
-- Name: kandidaturs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY kandidaturs
    ADD CONSTRAINT kandidaturs_pkey PRIMARY KEY (id);


--
-- Name: lands_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lands
    ADD CONSTRAINT lands_pkey PRIMARY KEY (id);


--
-- Name: parteis_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY parteis
    ADD CONSTRAINT parteis_pkey PRIMARY KEY (id);


--
-- Name: stimmzettels_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stimmzettels
    ADD CONSTRAINT stimmzettels_pkey PRIMARY KEY (id);


--
-- Name: wahlkreis_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY wahlkreis
    ADD CONSTRAINT wahlkreis_pkey PRIMARY KEY (id);


--
-- Name: wahls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY wahls
    ADD CONSTRAINT wahls_pkey PRIMARY KEY (id);


--
-- Name: index_kandidaturs_on_kandidat_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_kandidaturs_on_kandidat_id ON kandidaturs USING btree (kandidat_id);


--
-- Name: index_kandidaturs_on_partei_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_kandidaturs_on_partei_id ON kandidaturs USING btree (partei_id);


--
-- Name: index_kandidaturs_on_wahl_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_kandidaturs_on_wahl_id ON kandidaturs USING btree (wahl_id);


--
-- Name: index_stimmzettels_on_kandidat_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stimmzettels_on_kandidat_id ON stimmzettels USING btree (kandidat_id);


--
-- Name: index_stimmzettels_on_partei_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stimmzettels_on_partei_id ON stimmzettels USING btree (partei_id);


--
-- Name: index_stimmzettels_on_wahl_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stimmzettels_on_wahl_id ON stimmzettels USING btree (wahl_id);


--
-- Name: index_stimmzettels_on_wahlkreis_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stimmzettels_on_wahlkreis_id ON stimmzettels USING btree (wahlkreis_id);


--
-- Name: index_wahlkreis_on_land_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_wahlkreis_on_land_id ON wahlkreis USING btree (land_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20131115125204');

INSERT INTO schema_migrations (version) VALUES ('20131115125410');

INSERT INTO schema_migrations (version) VALUES ('20131115125740');

INSERT INTO schema_migrations (version) VALUES ('20131115125808');

INSERT INTO schema_migrations (version) VALUES ('20131115125843');

INSERT INTO schema_migrations (version) VALUES ('20131115130004');

INSERT INTO schema_migrations (version) VALUES ('20131115131528');
