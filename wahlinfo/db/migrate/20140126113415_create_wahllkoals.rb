class CreateWahllkoals < ActiveRecord::Migration
  def change
    create_table :wahllkoals do |t|
      t.references :wahlkreis, index: true
      t.string :wahlleiter
      t.string :adresse
      t.string :password_digest

      t.timestamps
    end
  end
end
