class CreateErststimmes < ActiveRecord::Migration
  def change
    create_table :erststimmes do |t|
      t.references :wahlkreis, index: true
      t.references :kandidat, index: true
      t.references :wahl, index: true

      t.timestamps
    end
  end
end
