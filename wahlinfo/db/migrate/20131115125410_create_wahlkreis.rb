class CreateWahlkreis < ActiveRecord::Migration
  def change
    create_table :wahlkreis do |t|
      t.string :name
      t.integer :wahlberechtigte
      t.references :land, index: true

      t.timestamps
    end
  end
end
