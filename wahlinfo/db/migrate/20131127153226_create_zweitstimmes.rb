class CreateZweitstimmes < ActiveRecord::Migration
  def change
    create_table :zweitstimmes do |t|
      t.references :wahlkreis, index: true
      t.references :partei, index: true
      t.references :wahl, index: true

      t.timestamps
    end
  end
end
