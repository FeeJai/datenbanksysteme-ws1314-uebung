class CreateStimmrechts < ActiveRecord::Migration
  def change
    create_table :stimmrechts do |t|
      t.references :wahllokal, index: true
      t.string :password
      t.boolean :used

      t.timestamps
    end
  end
end
