class CreateKandidaturs < ActiveRecord::Migration
  def change
    create_table :kandidaturs do |t|
      t.references :wahl, index: true
      t.references :kandidat, index: true
      t.integer :listenplatz
      t.references :partei, index: true
      t.references :partei, index: true
      t.references :partei, index: true

      t.timestamps
    end
  end
end
