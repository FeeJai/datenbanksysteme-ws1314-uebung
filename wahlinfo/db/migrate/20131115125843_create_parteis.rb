class CreateParteis < ActiveRecord::Migration
  def change
    create_table :parteis do |t|
      t.string :name

      t.timestamps
    end
  end
end
