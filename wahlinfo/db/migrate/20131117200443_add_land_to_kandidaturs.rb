class AddLandToKandidaturs < ActiveRecord::Migration
  def change
    add_reference :kandidaturs, :land, index: true
  end
end
