class CreateWahllokals < ActiveRecord::Migration
  def change
    create_table :wahllokals do |t|
      t.references :wahlkreis, index: true
      t.string :wahlleiter
      t.string :adresse
      t.string :password_hash
      t.string :password_salt

      t.timestamps
    end
  end
end
