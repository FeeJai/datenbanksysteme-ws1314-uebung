class CreateKandidats < ActiveRecord::Migration
  def change
    create_table :kandidats do |t|
      t.string :name
      t.text :adresse
      t.integer :geburtsjahr

      t.timestamps
    end
  end
end
