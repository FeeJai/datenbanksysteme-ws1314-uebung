class AddWahlkreisToKandidaturs < ActiveRecord::Migration
  def change
    add_reference :kandidaturs, :wahlkreis, index: true
  end
end
