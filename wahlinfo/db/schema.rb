# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140126212347) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "erststimmes", force: true do |t|
    t.integer  "wahlkreis_id"
    t.integer  "kandidat_id"
    t.integer  "wahl_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "erststimmes", ["kandidat_id"], name: "index_erststimmes_on_kandidat_id", using: :btree
  add_index "erststimmes", ["wahl_id"], name: "index_erststimmes_on_wahl_id", using: :btree
  add_index "erststimmes", ["wahlkreis_id"], name: "index_erststimmes_on_wahlkreis_id", using: :btree

  create_table "kandidats", force: true do |t|
    t.string   "name"
    t.text     "adresse"
    t.integer  "geburtsjahr"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kandidaturs", force: true do |t|
    t.integer  "wahl_id"
    t.integer  "kandidat_id"
    t.integer  "listenplatz"
    t.integer  "partei_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "land_id"
    t.integer  "wahlkreis_id"
  end

  add_index "kandidaturs", ["kandidat_id"], name: "index_kandidaturs_on_kandidat_id", using: :btree
  add_index "kandidaturs", ["land_id"], name: "index_kandidaturs_on_land_id", using: :btree
  add_index "kandidaturs", ["partei_id"], name: "index_kandidaturs_on_partei_id", using: :btree
  add_index "kandidaturs", ["wahl_id"], name: "index_kandidaturs_on_wahl_id", using: :btree
  add_index "kandidaturs", ["wahlkreis_id"], name: "index_kandidaturs_on_wahlkreis_id", using: :btree

  create_table "lands", force: true do |t|
    t.integer  "einwohner"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parteis", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stimmrechts", force: true do |t|
    t.integer  "wahllokal_id"
    t.string   "password"
    t.boolean  "used"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stimmrechts", ["wahllokal_id"], name: "index_stimmrechts_on_wahllokal_id", using: :btree

  create_table "stimmzettels", force: true do |t|
    t.integer  "wahlkreis_id"
    t.integer  "kandidat_id"
    t.integer  "partei_id"
    t.integer  "wahl_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stimmzettels", ["kandidat_id"], name: "index_stimmzettels_on_kandidat_id", using: :btree
  add_index "stimmzettels", ["partei_id"], name: "index_stimmzettels_on_partei_id", using: :btree
  add_index "stimmzettels", ["wahl_id"], name: "index_stimmzettels_on_wahl_id", using: :btree
  add_index "stimmzettels", ["wahlkreis_id"], name: "index_stimmzettels_on_wahlkreis_id", using: :btree

  create_table "wahlkreis", force: true do |t|
    t.string   "name"
    t.integer  "wahlberechtigte"
    t.integer  "land_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wahlkreis", ["land_id"], name: "index_wahlkreis_on_land_id", using: :btree

  create_table "wahllkoals", force: true do |t|
    t.integer  "wahlkreis_id"
    t.string   "wahlleiter"
    t.string   "adresse"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wahllkoals", ["wahlkreis_id"], name: "index_wahllkoals_on_wahlkreis_id", using: :btree

  create_table "wahllokals", force: true do |t|
    t.integer  "wahlkreis_id"
    t.string   "wahlleiter"
    t.string   "adresse"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wahllokals", ["wahlkreis_id"], name: "index_wahllokals_on_wahlkreis_id", using: :btree

  create_table "wahls", force: true do |t|
    t.integer  "jahr"
    t.integer  "sitze"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zweitstimmes", force: true do |t|
    t.integer  "wahlkreis_id"
    t.integer  "partei_id"
    t.integer  "wahl_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "zweitstimmes", ["partei_id"], name: "index_zweitstimmes_on_partei_id", using: :btree
  add_index "zweitstimmes", ["wahl_id"], name: "index_zweitstimmes_on_wahl_id", using: :btree
  add_index "zweitstimmes", ["wahlkreis_id"], name: "index_zweitstimmes_on_wahlkreis_id", using: :btree

end
