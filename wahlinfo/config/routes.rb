Wahlinfo::Application.routes.draw do

  get "mdb" => "mdb#index"
  get "mdb/:jahr" => "mdb#index"

  get "ueberhang/" => "ueberhang#index"
  get "ueberhang/:jahr" => "ueberhang#index"

  get "wahlkreissieger/"  => "wahlkreissieger#index"
  get "wahlkreissieger/:jahr"  => "wahlkreissieger#index"

  get "knapp/"  => "knapp#index"
  get "knapp/:jahr"  => "knapp#index"

  get "wkuebersicht/" => "wkuebersicht#index"
  get "wkuebersicht/:wk" => "wkuebersicht#index"

#  get "wahllokal/stimmrecht"
  match  "wahllokal/:action", :to => "wahllokal", via: [:get, :post]

  get "log_out" => "sessions#destroy", :as => "log_out"
  get "log_in" => "sessions#new", :as => "log_in"


  get "vote/" => "vote#index"
  match  "vote/:action", :to => "vote", via: [:get, :post]

  #match  "sessions/:action", :to => "sessions", via: [:get, :post]
  resources :sessions


  get "/:jahr/" => "home#index"


  #rails generate model stimmrecht wahllokal:references otp:


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
 
  root 'home#index'

  #match "login", :to => "sessions#login"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
