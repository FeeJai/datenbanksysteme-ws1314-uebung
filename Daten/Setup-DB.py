#!/usr/bin/python
# -*- coding: utf8 -*-

import csv, sys, psycopg2, os, getpass


print "1 - Erstmal alle Tabellen leeren"

#Open Connection to local database
con = None
try:
	con = psycopg2.connect(database='wahlinfo_dev', user=getpass.getuser()) 
	cur = con.cursor()

#	cur.execute('SELECT version()')          
#	ver = cur.fetchone()
#	print ver    

except psycopg2.DatabaseError, e:
    print 'Error %s' % e    
    sys.exit(1)


#cleanup the tables
cur.execute("TRUNCATE TABLE erststimmes RESTART IDENTITY ;")
cur.execute("TRUNCATE TABLE zweitstimmes RESTART IDENTITY ;")
con.commit()

cur.execute("TRUNCATE TABLE kandidats RESTART IDENTITY ;")
cur.execute("TRUNCATE TABLE kandidaturs RESTART IDENTITY ;")
cur.execute("TRUNCATE TABLE lands RESTART IDENTITY ;")
cur.execute("TRUNCATE TABLE parteis RESTART IDENTITY ;")
cur.execute("TRUNCATE TABLE wahlkreis RESTART IDENTITY ;")
cur.execute("TRUNCATE TABLE wahls RESTART IDENTITY ;")
con.commit()

print "2 - Statische Daten Importieren:"

path = os.path.dirname(os.path.realpath(__file__))

cur.execute("COPY kandidats (id, name, geburtsjahr) FROM '" + os.path.dirname(os.path.realpath(__file__))
 + "/CSV/kandidats.csv' DELIMITER ',' CSV HEADER Quote AS '\"'");
print " . kandidats"
con.commit()

cur.execute("COPY kandidaturs (wahl_id, kandidat_id, listenplatz, partei_id, land_id, wahlkreis_id) FROM '" + os.path.dirname(os.path.realpath(__file__)) +  "/CSV/kandidaturs.csv' DELIMITER ',' CSV ;");
print " . kandidaturs"
con.commit()

cur.execute("COPY lands (id, einwohner, name) FROM '" + os.path.dirname(os.path.realpath(__file__)) +  "/CSV/lands.csv' DELIMITER ';' CSV ;");
print " . lands"
con.commit()

cur.execute("COPY parteis (id, name) FROM '" + os.path.dirname(os.path.realpath(__file__)) + "/CSV/parteis.csv' DELIMITER ',' CSV ;");
print " . parteis"
con.commit()

cur.execute("COPY wahlkreis (id, name, wahlberechtigte, land_id) FROM '" + os.path.dirname(os.path.realpath(__file__)) + "/CSV/wahlkreis.csv' DELIMITER ';' CSV ;");
print " . wahlkreis"
con.commit()

cur.execute("COPY wahls (jahr, sitze) FROM '" + os.path.dirname(os.path.realpath(__file__)) + "/CSV/wahls.csv' DELIMITER ';' CSV ;");
print " . wahls"
con.commit()

print "3a - Wählerstimmen 2013 generieren:"

for x in range(1, 300):
	os.system("python Stimmzettelgenerator.py " + '%0*d' % (3, x) + " 1> /dev/null") # Mit zwei beginnenden Nullen


print "3b - Wählerstimmen 2009 generieren:"

for x in range(1, 300):
	os.system("python Stimmzettelgenerator2009.py " + '%0*d' % (3, x) + " 1> /dev/null") # Mit zwei beginnenden Nullen


print "4 - Views anlegen:"
con.autocommit = True
cur.execute(open("queries.sql", "r").read())

