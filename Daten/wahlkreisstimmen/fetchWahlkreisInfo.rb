#!/usr/bin/env ruby
require 'open-uri'

while ARGF.gets

wahlkreisdaten = $_.split(",");
wahlkreis = "00" + wahlkreisdaten[0];
wahlkreis = wahlkreis[wahlkreis.length-3,3];
bundesland = "0" + wahlkreisdaten[2].chop; 
bundesland = bundesland[bundesland.length-2,2];

url = "http://www.bundeswahlleiter.de/de/bundestagswahlen/BTW_BUND_13/ergebnisse/wahlkreisergebnisse/l"+bundesland+"/wk"+wahlkreis+"/"

html = open(url){ |f| f.read}
	
table = /<tbody>(.*)<\/tbody>/m.match(html)[1]
File.open(wahlkreis + ".csv", 'w') {|file| 
	table.scan(/<tr>(.*?)<\/tr>/m){|lines|
		entries = lines[0].scan(/<td.*?>(.*?)<\/td>/m)
		file.write(entries.join(";") + "\n")
	}
}	
end

