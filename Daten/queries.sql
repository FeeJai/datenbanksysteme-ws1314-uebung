-- CLEANUP First
DROP MATERIALIZED VIEW IF EXISTS ViewErststimmenAggWk    CASCADE;
DROP MATERIALIZED VIEW IF EXISTS ViewZweitstimmenAggWk   CASCADE;
DROP MATERIALIZED VIEW IF EXISTS ViewZweitstimmenAggLand CASCADE;
DROP MATERIALIZED VIEW IF EXISTS ViewZweitstimmenAggBund CASCADE;
DROP VIEW IF EXISTS Oberverteilung CASCADE;
DROP VIEW IF EXISTS Direktmandate CASCADE;
DROP VIEW IF EXISTS WkInfo CASCADE;
DROP FUNCTION IF EXISTS Landessitze(int) CASCADE;

-- Aggregierte Erststimmen
CREATE MATERIALIZED VIEW ViewErststimmenAggWk(wahlkreis_id, kandidat_id, stimmen) AS 
	SELECT wahlkreis_id, kandidat_id, COUNT(*) as stimmen, wahl_id
	FROM Erststimmes
	GROUP BY wahl_id, wahlkreis_id, kandidat_id
	ORDER BY wahl_id, wahlkreis_id, kandidat_id;

	--	SELECT * FROM ViewErststimmenAggWk


-- Aggregierte Zweitstimmen
CREATE MATERIALIZED VIEW ViewZweitstimmenAggWk(wahlkreis_id, partei_id, stimmen) AS 
	SELECT wahlkreis_id, partei_id, COUNT(*) as stimmen, wahl_id
	FROM Zweitstimmes
	GROUP BY wahl_id, wahlkreis_id, partei_id
	ORDER BY wahl_id, wahlkreis_id, partei_id;

	-- SELECT * FROM ViewZweitstimmenAggWk

CREATE MATERIALIZED VIEW ViewZweitstimmenAggLand(partei_id, land_id, stimmen) AS 
	SELECT partei_id, land_id, sum(stimmen) as stimmen, wahl_id
	FROM ViewZweitstimmenAggWk JOIN Wahlkreis 
	ON Wahlkreis.id = ViewZweitstimmenAggWk.wahlkreis_id
	GROUP BY wahl_id, partei_id, land_id;

	--  SELECT * FROM ViewZweitstimmenAggLand

CREATE MATERIALIZED VIEW ViewZweitstimmenAggBund(partei_id, stimmen_abs, stimmen_rel) AS 
	SELECT	partei_id,
		sum(stimmen) as stimmen_abs, 
		sum(stimmen) / (SELECT SUM(stimmen) FROM ViewZweitstimmenAggLand vzal2 WHERE vzal.wahl_id = vzal2.wahl_id) AS stimmen_rel,
		vzal.wahl_id
	FROM ViewZweitstimmenAggLand vzal
	GROUP BY wahl_id, partei_id
	ORDER BY wahl_id, partei_id;


	--  SELECT * FROM ViewZweitstimmenAggBund


-- Direktmandate, Gewinner der Wahlkreise. KandidatenId und Partei
CREATE VIEW Direktmandate AS
	WITH MaxErststimmenAggWk(wahlkreis_id, stimmen) AS (
		SELECT wahlkreis_id, MAX(stimmen) as stimmen, wahl_id
		FROM ViewErststimmenAggWk
		GROUP BY wahl_id, wahlkreis_id
		),

	WkGewinner AS (
		SELECT v.wahlkreis_id, v.kandidat_id, v.wahl_id
		FROM ViewErststimmenAggWk v JOIN MaxErststimmenAggWk max
		ON v.stimmen = max.stimmen AND v.wahlkreis_id = max.wahlkreis_id AND v.wahl_id = max.wahl_id
		)

	SELECT WkGewinner.*, wahlkreis.land_id, kandidaturs.partei_id FROM WkGewinner
		JOIN Wahlkreis ON Wahlkreis.id = WkGewinner.wahlkreis_id
		JOIN kandidaturs ON WkGewinner.kandidat_id = kandidaturs.kandidat_id  AND WkGewinner.wahl_id = kandidaturs.wahl_id;
		-- Es ist theoretisch möglich, auf einer anderen Landesliste zu kandidieren,
		-- als in dem Land, in dem der Wahlkreis eines Direktkandidaten sich befindet.
		-- Daher ist der Join über Wahlkreis notwendig, die Land_id aus `Kandidaturs`
		-- entspricht der Landesliste und ist ggf. inkorrekt


-- Sitze auf Länder verteilen
CREATE FUNCTION Landessitze(int) RETURNS TABLE(id int, name varchar, sitze bigint) AS '
	    WITH liste AS (
		SELECT id, name, (float4(einwohner))/((generate_series (1,$1))-0.5) as rangmasszahl 
		FROM lands 
		order by rangmasszahl DESC 
		LIMIT $1
		)

	SELECT id, name, count (rangmasszahl) as sitze
	FROM liste
	GROUP BY id, name
	ORDER BY id ASC
	'
 LANGUAGE SQL;

 	-- SELECT * FROM Landessitze(598)


-- Q1: Hier gibt es die Sitze pro Partei
CREATE VIEW Oberverteilung (id, name, sitze, wahl_id) AS
	WITH Sperrklausel AS
	    (SELECT partei_id, wahl_id
	    FROM ViewZweitstimmenAggBund
	    WHERE stimmen_rel >= 0.05  -- 5% Hürde
	    UNION
	    -- Grundmandatsklausel
	    SELECT partei_id, wahl_id
	    FROM direktmandate
	    GROUP BY wahl_id, partei_id
	    HAVING COUNT(*) >= 3
	    ),

	-- Mindestsitzzahl und Bundesdivisor
	Gesamtstimmen_Land AS(
		SELECT land_id, sum(stimmen) as gesamtstimmen, Sperrklausel.wahl_id as wahl_id
		FROM Sperrklausel 
		JOIN ViewZweitstimmenAggLand
			ON ViewZweitstimmenAggLand.partei_id = Sperrklausel.partei_id
			AND Sperrklausel.wahl_id = ViewZweitstimmenAggLand.wahl_id
		GROUP BY Sperrklausel.wahl_id, land_id
		),

	Landeswerte AS (
	SELECT id, sitze, gesamtstimmen, wahl_id
		FROM Landessitze(598) as lsitz
		JOIN Gesamtstimmen_Land as gland
		ON gland.land_id = lsitz.id
		),

	Parteiwerte AS
		(SELECT ViewZweitstimmenAggLand.partei_id, land_id, ViewZweitstimmenAggLand.stimmen, ViewZweitstimmenAggLand.wahl_id
		 FROM Sperrklausel 
		 JOIN ViewZweitstimmenAggLand
			ON ViewZweitstimmenAggLand.partei_id = Sperrklausel.partei_id
			AND ViewZweitstimmenAggLand.wahl_id = Sperrklausel.wahl_id
		),

	SitzeKombiniertListeDirekt AS
		(
		SELECT land_id, partei_id, (round(sitze*stimmen/gesamtstimmen)) AS sitze, pw.wahl_id
			FROM Parteiwerte pw
			JOIN Landeswerte lw
			ON pw.land_id = lw.id
			AND pw.wahl_id = lw.wahl_id
		UNION ALL
		SELECT land_id, partei_id, COUNT (kandidat_id) as sitze, wahl_id
			FROM Direktmandate
			GROUP BY wahl_id, land_id, partei_id
		),

	MinSitzeLand AS
		(SELECT land_id, partei_id, max(sitze) as sitze, wahl_id
		FROM SitzeKombiniertListeDirekt
		GROUP BY land_id, partei_id, wahl_id),

	MinSitzeBund AS
		(SELECT partei_id, sum(sitze) as sitze, wahl_id
		FROM MinSitzeLand
		GROUP BY partei_id, wahl_id),

	Bundesdivisor AS 
		(SELECT MIN(floor(zab.stimmen_abs/(sitze-0.5))) AS bundesdivisor, msb.wahl_id
		FROM MinSitzeBund msb
		JOIN ViewZweitstimmenAggBund zab
		ON msb.partei_id = zab.partei_id
		AND  msb.wahl_id = zab.wahl_id
		GROUP BY msb.wahl_id)

	-- Jetzt die Site für die Parteien berechnen
	SELECT zab.partei_id, p.name, round(stimmen_abs/bundesdivisor) as sitze, zab.wahl_id
	FROM ViewZweitstimmenAggBund zab
	JOIN Sperrklausel sk ON zab.partei_id = sk.partei_id AND sk.wahl_id = zab.wahl_id
	JOIN parteis p ON p.id = zab.partei_id
	JOIN Bundesdivisor ON zab.wahl_id = Bundesdivisor.wahl_id
	ORDER BY zab.partei_id;

-- Q3.1 und Q3.2: Wahlbeteiligung und Direktkandidaten pro Wahlkreis
CREATE VIEW WkInfo (id, wahlbeteiligung, kandidat_id, name, partei_id, wahl_id) AS

	WITH MaxErststimmenAggWk(wahlkreis_id, stimmen) AS (
		SELECT wahlkreis_id, MAX(stimmen) as stimmen, wahl_id
		FROM ViewErststimmenAggWk
		GROUP BY wahlkreis_id, wahl_id
		),

	WkGewinner AS (
		SELECT v.wahlkreis_id, v.kandidat_id, v.wahl_id
		FROM ViewErststimmenAggWk v
		JOIN MaxErststimmenAggWk max
		ON v.stimmen = max.stimmen AND v.wahlkreis_id = max.wahlkreis_id AND  v.wahl_id = max.wahl_id
		),

	ErststimmenWK AS (
		SELECT wahlkreis_id, SUM(stimmen) as stimmen, wahl_id
		FROM ViewErststimmenAggWk
		GROUP BY wahlkreis_id, wahl_id
		),
		
	ZweitstimmenWK AS (
		SELECT wahlkreis_id, SUM(stimmen) as stimmen, wahl_id
		FROM ViewZweitstimmenAggWk
		GROUP BY wahlkreis_id, wahl_id
		),
		
	Wahlteilnehmer AS
		(SELECT wahlkreis_id, MAX(stimmen) as teilnehmer, wahl_id
		FROM
			(SELECT * FROM ErststimmenWK
			UNION ALL
			SELECT * FROM ZweitstimmenWK
			ORDER BY wahlkreis_id
			) erstundzweitstimmen
		GROUP BY wahl_id, wahlkreis_id)

	SELECT Wahlteilnehmer.wahlkreis_id, (teilnehmer / wahlberechtigte) as Wahlbeteiligung, kandidat_id, kandidats.name, partei_id, Wahlteilnehmer.wahl_id
	FROM Wahlteilnehmer
	JOIN wahlkreis 
	ON wahlkreis.id = Wahlteilnehmer.wahlkreis_id
	JOIN Direktmandate
	ON wahlkreis.id = Direktmandate.wahlkreis_id
	AND Wahlteilnehmer.wahl_id = Direktmandate.wahl_id
	JOIN Kandidats
	ON Direktmandate.kandidat_id = kandidats.id
	ORDER BY wahlkreis_id;

-- Jetzt noch Leserechte für das Rails Projekt

GRANT SELECT ON TABLE ViewErststimmenAggWk TO public;
GRANT SELECT ON TABLE ViewZweitstimmenAggWk TO public;
GRANT SELECT ON TABLE ViewZweitstimmenAggLand TO public;
GRANT SELECT ON TABLE ViewZweitstimmenAggBund TO public;
GRANT SELECT ON TABLE Direktmandate TO public;
GRANT SELECT ON TABLE Oberverteilung TO public;
GRANT SELECT ON TABLE WkInfo TO public;
