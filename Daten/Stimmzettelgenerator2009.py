#!/usr/bin/python
# -*- coding: utf8 -*-
""" Algorithmus:
1. Öffne csv-Datei WahlkreisID.csv
2. Mit Datenbank verbinden
3. Für jede Zeile:
	1. Spalte einlesen
	2. Fallunterscheidung:
		A. Wahlberechtigte, Wähler, Gültige
		B. Ungültige
			1. Spalte 2 einlesen
			2. generate_sequence: wahlkreis_id, null in erststimmes
			3. Spalte 5 einlesen
			4. generate_sequence: wahlkreis_id, null in zweitstimmes

		C. "K:%"
			1. KandidatenID in Kandidaturs-Tabelle nachschlagen
			2. Spalte 5 einlesen
			3. Stimmen mit genereate_sequence into zweistimmes erzeugen

		D. default
			1. ParteiID und KandidatenID für Wahlkreis nachschlagen
			2. generate_sequence: wahlkreis_id, parteiid in erststimmes
			3. Spalte 5 einlesen
			4. generate_sequence: wahlkreis_id, KandidatenID in zweitstimmes
"""

import csv, sys, psycopg2

wahl = "2009"

wahlkreisID = sys.argv[1]
print "Importing File " + wahlkreisID

#Open Connection to local database
con = None
try:
	con = psycopg2.connect(database='wahlinfo_dev', user='Wahlinfo') 
	cur = con.cursor()

#	cur.execute("TRUNCATE TABLE erststimmes ;")
#	cur.execute("TRUNCATE TABLE zweitstimmes ;")
#	con.commit()

#	cur.execute('SELECT version()')          
#	ver = cur.fetchone()
#	print ver    

except psycopg2.DatabaseError, e:
    print 'Error %s' % e    
    sys.exit(1)


#cur.execute('INSERT INTO erststimmes (wahlkreis_id, kandidat_id, wahl_id) SELECT 001 AS wahlkreis_id, null AS kandidat_id, 1 AS wahl_id FROM generate_Series(1,2223) AS i;');
#con.commit()



with open('./wahlkreise2009/' + wahlkreisID + '.csv', 'rb') as csvfile:
	csvreader = csv.reader(csvfile, delimiter=',')
	for row in csvreader:		
		name = row[0]
		erststimmen = str(int(int(row[1].replace(" ","").replace(".","").replace("-","0").replace("keineerst","0"))))
		zweitstimmen = str(int(int(row[2].replace(" ","").replace(".","").replace("-","0"))))

		if (name == "Wahlberechtigte") or (name == "Wähler") or (name == "Gültige"):
			pass
		elif (name == "Ungültige"):
			print "Ungültige Stimmen in Wahlkreis " + wahlkreisID + ": " + erststimmen + " Erststimmen u. " + zweitstimmen + " Zweitstimmen"
			cur.execute("INSERT INTO erststimmes (wahlkreis_id, kandidat_id, wahl_id) SELECT " + wahlkreisID + " AS wahlkreis_id, null AS kandidat_id, " + wahl + " AS wahl_id FROM generate_Series(1," + erststimmen + ") AS i;")
			cur.execute("INSERT INTO zweitstimmes (wahlkreis_id, partei_id, wahl_id) SELECT " + wahlkreisID + " AS wahlkreis_id, null AS partei_id, " + wahl + " AS wahl_id FROM generate_Series(1," + zweitstimmen + ") AS i;")
			con.commit()
		elif (name[:2] == "K:"):
			cur.execute("SELECT kandidat_id from kandidats JOIN kandidaturs ON kandidats.id = kandidaturs.kandidat_id where wahlkreis_id = " + wahlkreisID + " AND name LIKE '" + name[2:] + "%' AND wahl_id = " + wahl)
			KandidatenID = cur.fetchone()[0]

			print (erststimmen + " Erststimmen für parteilosen Direktkandidaten " +  name[2:] + " (" + str(KandidatenID) + ") in Wahlkreis " + wahlkreisID)
			cur.execute("INSERT INTO erststimmes (wahlkreis_id, kandidat_id, wahl_id) SELECT " + wahlkreisID + " AS wahlkreis_id, " + str(KandidatenID) + " AS kandidat_id, " + wahl + " AS wahl_id FROM generate_Series(1," + erststimmen + ") AS i;")
			con.commit()

		else:
			#Zweistimmen für Partei - PareiID finden
			cur.execute("SELECT id from parteis where name = '" + name + "';")
			if cur.rowcount != 1:
				sys.stderr.write("FEHLER: Partei " + name + " nicht gefunden.  Wahlkreis: " + wahlkreisID + " \n")
				continue
			ParteiID = str(cur.fetchone()[0])
			print (zweitstimmen + " Zweitstimmen für Partei " +  name + "  in Wahlkreis " + wahlkreisID)
			#print("INSERT INTO zweitstimmes (wahlkreis_id, partei_id, wahl_id) SELECT " + wahlkreisID + " AS wahlkreis_id, " + ParteiID + " AS partei_id, " + wahl + " AS wahl_id FROM generate_Series(1," + zweitstimmen + ") AS i;")

			cur.execute("INSERT INTO zweitstimmes (wahlkreis_id, partei_id, wahl_id) SELECT " + wahlkreisID + " AS wahlkreis_id, " + ParteiID + " AS partei_id, " + wahl + " AS wahl_id FROM generate_Series(1," + zweitstimmen + ") AS i;")
			con.commit()

			#Erststimmen für Partei - Kandidaten finden
			if ((erststimmen.isdigit()) and (int(erststimmen) >= 1)):
				cur.execute("SELECT kandidat_id from parteis JOIN kandidaturs ON parteis.id = kandidaturs.partei_id where wahlkreis_id = " + wahlkreisID + " AND partei_id = '" + ParteiID + "' AND wahl_id = " + wahl)
				if (cur.rowcount == 0):
					sys.stderr.write("FEHLER: Kein Direktkandidat für Partei " + name + " in Wahlkreis " + wahlkreisID + " gefunden. Erststimmen: " + erststimmen + "\n")

				else:
					KandidatenID = str(cur.fetchone()[0])

					print (erststimmen + " Erststimmen für Kandidat " + KandidatenID + " der Partei " + name + " in Wahlkreis " + wahlkreisID)
					cur.execute("INSERT INTO erststimmes (wahlkreis_id, kandidat_id, wahl_id) SELECT " + wahlkreisID + " AS wahlkreis_id, " + str(KandidatenID) + " AS kandidat_id, " + wahl + " AS wahl_id FROM generate_Series(1," + erststimmen + ") AS i;")
					con.commit()
			else:
				print ("Kein Direktkandidat der Partei " + name + " in Wahlkreis " + wahlkreisID)
		#print '; '.join(row)

if con:
	con.close()


