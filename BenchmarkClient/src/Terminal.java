import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class Terminal implements Runnable{

    HttpResponse response;
    HttpEntity entity;
    DefaultHttpClient httpClient;

    int t;
    int loop;
    StringBuffer s = new StringBuffer();
    long elapsedTime;
    String a = null;
    static ArrayList liste = new ArrayList();
    static ConcurrentHashMap<String, Long> hm = new ConcurrentHashMap();
    static ConcurrentHashMap<String, Integer> hi = new ConcurrentHashMap<String, Integer>();
    static List<Long> time = new ArrayList<Long>();
    static List<Integer> freq = new ArrayList<Integer>();
    static List newl = new ArrayList();
    static List<String> URLS = new ArrayList<String>();



    public Terminal(int t, int loop) {
        this.t = t;
        this.loop = loop;
    }



    // Calculate random time interval
    public double sleepRandom(){
        return (Math.random() * (1.2 - 0.8) + 0.8) * t * 1000;
    }

    // Get random Q according to probability
    /*
    Q1 0 to 25
    Q2 26 to 35
    Q3 36 to 60
    Q4 61 to 70
    Q5 71 to 80
    Q6 81 to 100
     */
    public Request getRequest(){
        double p = Math.random();
        double cumulativeProbability = 0.0;
        Request request = null;
        //String URL = null;
        for (Request req : Request.values()) {
            cumulativeProbability += req.getProbability();
            if (p <= cumulativeProbability) {
                request = req;
                break;
            }
        }
        return request;
    }

    int i = 0;
    long val = 0;

    @Override
    public void run() {
        try{

            hm.put("Q1", val);
            hm.put("Q2", val);
            hm.put("Q3", val);
            hm.put("Q4", val);
            hm.put("Q5", val);
            hm.put("Q6", val);

            hi.put("Q1", 0);
            hi.put("Q2", 0);
            hi.put("Q3", 0);
            hi.put("Q4", 0);
            hi.put("Q5", 0);
            hi.put("Q6", 0);



            StringBuffer buf = null;



            System.out.println("HI - running");

            System.out.println(getRequest().getURL());
        for(int i = 0; i < loop; i++){

            httpClient = new DefaultHttpClient();

            long startTime = System.currentTimeMillis();
            System.out.println("Current time is " + startTime);

            Request request = getRequest();

            HttpGet httpget = new HttpGet(request.getURL());

            System.out.println("executing request" + httpget.getRequestLine());
            response = httpClient.execute(httpget);
            entity = response.getEntity();




        System.out.println("----------------------------------------");
        System.out.println(response.getStatusLine());
        if (entity != null) {
            System.out.println("Response content length: " + entity.getContentLength());

            elapsedTime = System.currentTimeMillis() - startTime;
            synchronized (this){
              liste.add("Terminal ID: " + Thread.currentThread().getId() + "; Elapsed time: " + elapsedTime + " ms ;" + request.getID() + ": " + request.getURL() + "\n");
              hm.put(request.getID(), hm.get(request.getID()) + elapsedTime);
              hi.put(request.getID(), hi.get(request.getID()) + 1);


            }
            System.out.println("Total elapsed http request/response time in milliseconds for " + request.getID() + " is: " + elapsedTime + " ms");


            s.append(String.valueOf(request.getID() + " " + elapsedTime + " ms" + " , "));

            System.out.println("Calling " + s + " in Thread " + Thread.currentThread().getId());

        }

            System.out.println("Sleeping");

            long time = (long) sleepRandom();
            System.out.println("Sleeping " + time);
            Thread.sleep(time);
        }


            time = new ArrayList(hm.values());
            freq = new ArrayList(hi.values());
            URLS = new ArrayList<String>(hi.keySet());

            newl = new ArrayList();

            for (int i=0; i < time.size(); i++)
            {
                if (freq.get(i)!= 0)
                    newl.add(time.get(i)/freq.get(i) + " ms");
                else
                    newl.add(i, 0);
            }



        }

        catch(Exception E){
           // httpClient.getConnectionManager().shutdown();
           System.out.println("Error");
           E.printStackTrace();
        }



           }
    }





