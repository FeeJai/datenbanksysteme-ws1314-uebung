import java.io.*;
import java.util.*;

public class Benchmark {

    public static void main(String[] args) throws InterruptedException, IOException{


        List<Thread> term = new ArrayList<Thread>();
        int n = Integer.parseInt(args[0]);
        List results = new ArrayList();

        int t = Integer.parseInt(args[1]);

        for (int i = 0; i < n; i++){
            Thread thread = new Thread(new Terminal(t, 5));
            term.add(thread);
            thread.start();
        }

        for (int i = 0; i < term.size(); i++){
            term.get(i).join();
        }

        System.out.println("In Benchmark ");
        System.out.println(Terminal.liste);


            for (int i = 0; i < Terminal.time.size(); i++) {
                results.add(Terminal.URLS.get(i) + ": ");
                results.add(Terminal.newl.get(i) + "; ");
            }
            results.add("\n");

            System.out.println("This is results " + results);

        File file = new File("Benchmark" + n + "Terminals" + "Wartezeit " + t + ".csv");
        //if file doesnt exists, then create it
        if(!file.exists()){
            file.createNewFile();
        }


        FileWriter fileWriter = new FileWriter(file.getName());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            //bufferedWriter.write("ID " + Thread.currentThread().getId() + ", "  + s + "\n");
            bufferedWriter.write(Terminal.liste.toString().replace("[", "").replace("]", "").replace(", ", "") + " Average request time " + "\n"
                    + results.toString().replace("[", "").replace("]", "").replace(", ", ""));
            //bufferedWriter.write(results.toString().replace("[", "").replace("]", "").replace(", ", ""));            //bufferedWriter.write(results.toString().replace("[", "").replace("]", "").replace(", ", ""));


        bufferedWriter.close();
        System.out.println(Terminal.liste.toString().replace("[", "").replace("]", "").replace(", ", ""));


    }
}