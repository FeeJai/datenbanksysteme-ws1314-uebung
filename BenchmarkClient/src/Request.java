public enum Request {

    Q1("http://0.0.0.0:3000/", 0.25, "Q1"),
    Q2("http://0.0.0.0:3000/mdb", 0.10, "Q2"),
    Q3("http://0.0.0.0:3000/wkuebersicht", 0.25, "Q3"),
    Q4("http://0.0.0.0:3000/wahlkreissieger", 0.1, "Q4"),
    Q5("http://0.0.0.0:3000/ueberhang", 0.1, "Q5"),
    Q6("http://0.0.0.0:3000/knapp", 0.20, "Q6");



    String URL;
    double probability;
    String ID;

    Request(String URL, double probability, String ID) {
        this.URL = URL;
        this.probability = probability;
        this.ID = ID;

    }


    public String getURL() {
        return URL;
    }


    public double getProbability() {
        return probability;
    }

    public String getID() {
        return ID;
    }


}

