-- other-queries


-- Q3.3: Prozentuale und Absolute Anzahl Zweitstimmen
WITH Zweitstimmen AS
	(SELECT partei_id, stimmen, wahl_id
	FROM ViewZweitstimmenAggWk
	WHERE wahlkreis_id = 42),

Gesamtstimmen AS
	(SELECT SUM(stimmen) as gesamt, wahl_id
	FROM Zweitstimmen
	GROUP BY wahl_id)

SELECT partei_id, stimmen as stimmen_absolut, (stimmen/gesamtstimmen.gesamt) as stimmen_relativ, Gesamtstimmen.wahl_id
FROM Zweitstimmen, Gesamtstimmen
WHERE Zweitstimmen.wahl_id = Gesamtstimmen.wahl_id



-- Q4: Wahlkreissieger
WITH MaxErststimmenAggWk(wahlkreis_id, stimmen) AS
	(SELECT wahlkreis_id, MAX(stimmen) as stimmen
	FROM ViewErststimmenAggWk
	WHERE wahl_id = ?
	GROUP BY wahlkreis_id
	),
	
WkGewinner AS 
	(SELECT v.wahlkreis_id, v.kandidat_id, k.partei_id
	FROM ViewErststimmenAggWk v
	JOIN MaxErststimmenAggWk max
		ON v.stimmen = max.stimmen AND v.wahlkreis_id = max.wahlkreis_id
	JOIN kandidaturs k
		ON  v.kandidat_id = k.kandidat_id
	WHERE  v.wahl_id = k.wahl_id),
	
MaxZweitstimmenAggWk(wahlkreis_id, stimmen) AS
	(SELECT wahlkreis_id, MAX(stimmen) as stimmen
	FROM ViewZweitstimmenAggWk
	GROUP BY wahlkreis_id
	),
	
ParteiMaxZweitstimmen AS 
	(SELECT v.wahlkreis_id, v.partei_id
	FROM ViewZweitstimmenAggWk v
	JOIN MaxZweitstimmenAggWk max
		ON v.stimmen = max.stimmen AND v.wahlkreis_id = max.wahlkreis_id
	)
	
SELECT erst.wahlkreis_id, kandidat_id as Direktkandidat, erst.partei_id as ParteiDirektkandiat, zweit.partei_id as ParteiMeisteZweitstimmen
FROM WkGewinner erst
JOIN ParteiMaxZweitstimmen zweit
ON erst.wahlkreis_id = zweit.wahlkreis_id
ORDER BY erst.wahlkreis_id