
-- Q6: Knappste Sieger
WITH MaxErststimmenAggWk(wahlkreis_id, stimmen) AS
	(SELECT wahlkreis_id, MAX(stimmen) as stimmen, wahl_id
	FROM ViewErststimmenAggWk
	WHERE wahl_id = 2013 -- !!
	GROUP BY wahlkreis_id, wahl_id
	),
	
WkGewinner AS 
	(SELECT v.wahlkreis_id, v.kandidat_id, max.stimmen, v.wahl_id
	FROM ViewErststimmenAggWk v
	JOIN MaxErststimmenAggWk max
	ON v.stimmen = max.stimmen
	AND v.wahlkreis_id = max.wahlkreis_id
	AND v.wahl_id = max.wahl_id
	),

WahlkreisZweiterStimmen AS
	(SELECT eawk.wahlkreis_id, MAX(stimmen) as stimmen, wahl_id
	FROM ViewErststimmenAggWk eawk
	WHERE wahl_id = 2013
	AND eawk.kandidat_id NOT IN (select kandidat_id from Direktmandate WHERE wahl_id = 2013)
 	GROUP BY eawk.wahlkreis_id, wahl_id),

 WahlkreisZweiter AS
 	(SELECT eawk.wahlkreis_id, eawk.kandidat_id, eawk.stimmen
	FROM ViewErststimmenAggWk eawk, WahlkreisZweiterStimmen wkzs
	WHERE eawk.stimmen = wkzs.stimmen
	AND eawk.wahlkreis_id = wkzs.wahlkreis_id),

Vorsprung (kandidat_id, stimmdifferenz) AS
	(SELECT wkg.kandidat_id, wkg.stimmen - wkz.stimmen
	FROM WkGewinner wkg, WahlkreisZweiter wkz
	WHERE wkg.wahlkreis_id = wkz.wahlkreis_id
	),
		
Gewinnerranking AS
	(SELECT ku.wahlkreis_id, k.name, ku.partei_id, v.stimmdifferenz, 
		rank() OVER (PARTITION BY partei_id ORDER BY v.stimmdifferenz) as Rang
	FROM Vorsprung v, kandidats k, kandidaturs ku
	WHERE k.id = v.kandidat_id 
	AND ku.kandidat_id = k.id
	AND ku.wahl_id = 2013)


--SELECT * FROM Gewinnerranking ORDER BY Rang
SELECT *
FROM Gewinnerranking
WHERE Gewinnerranking.Rang <= 10;


-- knappste verlierer
WITH Sperrklausel AS
    (SELECT * FROM (SELECT partei_id, wahl_id
    FROM ViewZweitstimmenAggBund
    WHERE stimmen_rel >= 0.05  -- 5% Hürde
    UNION
    -- Grundmandatsklausel
    SELECT partei_id, wahl_id
    FROM direktmandate
    GROUP BY wahl_id, partei_id
    HAVING COUNT(*) >= 3
    ) AS sk_temp 
    WHERE wahl_id = 2013),  -- !

ParteienOhneDirektmandat AS (
--	SELECT partei_id FROM Sperrklausel 
	SELECT id as partei_id FROM parteis
	WHERE id NOT IN (select partei_id from Direktmandate where wahl_id = 2013) -- !!
	),

MaxErststimmenAggWk(wahlkreis_id, stimmen) AS
	(SELECT wahlkreis_id, MAX(stimmen) as stimmen, wahl_id
	FROM ViewErststimmenAggWk
	WHERE wahl_id = 2013 -- !!
	GROUP BY wahlkreis_id, wahl_id
	),
	
WkGewinner AS 
	(SELECT v.wahlkreis_id, v.kandidat_id, max.stimmen, v.wahl_id
	FROM ViewErststimmenAggWk v
	JOIN MaxErststimmenAggWk max
	ON v.stimmen = max.stimmen
	AND v.wahlkreis_id = max.wahlkreis_id
	AND v.wahl_id = max.wahl_id
	),

Verlierer AS 
	(SELECT k.id as kandidat_id, ku.wahlkreis_id, ku.partei_id
	FROM kandidats k, kandidaturs ku
	WHERE ku.kandidat_id = k.id
	AND wahlkreis_id IS NOT NULL
	AND wahl_id = 2013
	AND k.id NOT IN (SELECT kandidat_id FROM Direktmandate)
	AND ku.partei_id IN (SELECT partei_id FROM ParteienOhneDirektmandat)),

Unterschied AS (
  	SELECT v.wahlkreis_id, v.kandidat_id, v.partei_id, wkg.stimmen - eaw.stimmen as stimmdifferenz
  	FROM WkGewinner wkg, ViewErststimmenAggWk eaw, Verlierer v
  	WHERE v.kandidat_id = eaw.kandidat_id 
  	AND v.wahlkreis_id = wkg.wahlkreis_id 
  	AND wkg.wahl_id = eaw.wahl_id
),

Verliererranking AS
	(SELECT ku.wahlkreis_id, k.name, ku.partei_id, v.stimmdifferenz, rank() OVER (PARTITION BY ku.partei_id ORDER BY v.stimmdifferenz) as Rang
		FROM Unterschied v, kandidats k, kandidaturs ku
		WHERE k.id = v.kandidat_id AND ku.kandidat_id = k.id)


SELECT *
FROM Verliererranking
WHERE Verliererranking.Rang <= 10;
