WITH Sperrklausel AS
    (SELECT partei_id, wahl_id
    FROM ViewZweitstimmenAggBund
    WHERE stimmen_rel >= 0.05  -- 5% Hürde
    UNION
    -- Grundmandatsklausel
    SELECT partei_id, wahl_id
    FROM direktmandate
    GROUP BY wahl_id, partei_id
    HAVING COUNT(*) >= 3
    ),

-- Mindestsitzzahl und Bundesdivisor
Gesamtstimmen_Land AS(
	SELECT land_id, sum(stimmen) as gesamtstimmen, Sperrklausel.wahl_id as wahl_id
	FROM Sperrklausel 
	JOIN ViewZweitstimmenAggLand
		ON ViewZweitstimmenAggLand.partei_id = Sperrklausel.partei_id
		AND Sperrklausel.wahl_id = ViewZweitstimmenAggLand.wahl_id
	GROUP BY Sperrklausel.wahl_id, land_id
	),

Landeswerte AS (
SELECT id, sitze, gesamtstimmen, wahl_id
	FROM Landessitze(598) as lsitz
	JOIN Gesamtstimmen_Land as gland
	ON gland.land_id = lsitz.id
	),

Parteiwerte AS
	(SELECT ViewZweitstimmenAggLand.partei_id, land_id, ViewZweitstimmenAggLand.stimmen, ViewZweitstimmenAggLand.wahl_id
	 FROM Sperrklausel 
	 JOIN ViewZweitstimmenAggLand
		ON ViewZweitstimmenAggLand.partei_id = Sperrklausel.partei_id
		AND ViewZweitstimmenAggLand.wahl_id = Sperrklausel.wahl_id
	),

SitzeKombiniertListeDirekt AS
	(
	SELECT land_id, partei_id, (round(sitze*stimmen/gesamtstimmen)) AS sitze, pw.wahl_id
		FROM Parteiwerte pw
		JOIN Landeswerte lw
		ON pw.land_id = lw.id
		AND pw.wahl_id = lw.wahl_id
	UNION ALL
	SELECT land_id, partei_id, COUNT (kandidat_id) as sitze, wahl_id
		FROM Direktmandate
		GROUP BY wahl_id, land_id, partei_id
	),

MinSitzeLand AS
	(SELECT land_id, partei_id, max(sitze) as sitze, wahl_id
	FROM SitzeKombiniertListeDirekt
	GROUP BY land_id, partei_id, wahl_id),

MinSitzeBund AS
	(SELECT partei_id, sum(sitze) as sitze, wahl_id
	FROM MinSitzeLand
	GROUP BY partei_id, wahl_id),

Bundesdivisor AS 
	(SELECT MIN(floor(zab.stimmen_abs/(sitze-0.5))) AS bundesdivisor, msb.wahl_id
	FROM MinSitzeBund msb
	JOIN ViewZweitstimmenAggBund zab
	ON msb.partei_id = zab.partei_id
	AND  msb.wahl_id = zab.wahl_id
	GROUP BY msb.wahl_id),

-- Jetzt die Site für die Parteien berechnen
DirektmandateProLand AS
	(SELECT land_id, partei_id, COUNT (kandidat_id) as sitze
	FROM Direktmandate
	WHERE wahl_id = 2009 -- !!!!
	GROUP BY land_id, partei_id),
	
Divisoren(divisor) AS (
    SELECT * 
    FROM generate_series(1,300,2)
),

Unterverteilung AS (
	SELECT z.land_id, z.partei_id, 
		RANK() OVER (PARTITION BY z.partei_id ORDER BY z.stimmen/d.divisor DESC) as rangmasszahl
	FROM Divisoren d, ViewZweitstimmenAggLand z
	WHERE z.wahl_id = 2013
),

-- 1. Durchlauf Überhangmandate berechnen
Oberverteilung1 AS
	(SELECT zab.partei_id, p.name, round(stimmen_abs/bundesdivisor) as sitze, zab.wahl_id
	FROM ViewZweitstimmenAggBund zab
	JOIN Sperrklausel sk ON zab.partei_id = sk.partei_id AND sk.wahl_id = zab.wahl_id
	JOIN parteis p ON p.id = zab.partei_id
	JOIN Bundesdivisor ON zab.wahl_id = Bundesdivisor.wahl_id
	WHERE zab.wahl_id = 2009 -- !!!!!
	ORDER BY zab.partei_id),

GewaehlteMandateProLand1 AS (
    SELECT m.land_id, m.partei_id, COUNT(*) as sitze
    FROM Unterverteilung m, Oberverteilung1 ov
    WHERE m.rangmasszahl <= ov.sitze
    AND m.partei_id = ov.partei_id
    GROUP BY m.land_id, m.partei_id
),

Ueberhangmandate1 AS (
	SELECT dm.land_id, dm.partei_id, dm.sitze - lm.sitze as Anzahl
		FROM DirektmandateProLand dm
		JOIN GewaehlteMandateProLand1 lm 
		ON dm.land_id = lm.land_id
		AND dm.partei_id = lm.partei_id
		WHERE dm.sitze - lm.sitze > 0),

SumUeberhangmandate1 AS (
	SELECT partei_id, SUM(anzahl) 
	FROM Ueberhangmandate1
	GROUP BY partei_id),

-- Überhangmandate intern ausgleichen
-- 2. Durchlauf Überhangmandate neu berechen

Oberverteilung2 AS
	(SELECT ov.partei_id, name, (sitze - coalesce(sum, 0)) as sitze
	FROM Oberverteilung1 ov
	FULL JOIN SumUeberhangmandate1 um
	ON ov.partei_id = um.partei_id),

GewaehlteMandateProLand2 AS (
    SELECT m.land_id, m.partei_id, COUNT(*) as sitze
    FROM Unterverteilung m, Oberverteilung2 ov
    WHERE m.rangmasszahl <= ov.sitze
    AND m.partei_id = ov.partei_id
    GROUP BY m.land_id, m.partei_id
),

Ueberhangmandate2 AS (
	SELECT dm.land_id, dm.partei_id, dm.sitze - lm.sitze as Anzahl
		FROM DirektmandateProLand dm
		JOIN GewaehlteMandateProLand2 lm 
		ON dm.land_id = lm.land_id
		AND dm.partei_id = lm.partei_id
		WHERE dm.sitze - lm.sitze > 0),

SumUeberhangmandate2 AS (
	SELECT partei_id, SUM(anzahl) 
	FROM Ueberhangmandate2
	GROUP BY partei_id),


-- erneut intern ausgleichen
-- und zum dritten mal berechnen

Oberverteilung3 AS
	(SELECT ov.partei_id, name, (sitze - coalesce(sum, 0)) as sitze
	FROM Oberverteilung2 ov
	FULL JOIN SumUeberhangmandate2 um
	ON ov.partei_id = um.partei_id),

GewaehlteMandateProLand3 AS (
    SELECT m.land_id, m.partei_id, COUNT(*) as sitze
    FROM Unterverteilung m, Oberverteilung3 ov
    WHERE m.rangmasszahl <= ov.sitze
    AND m.partei_id = ov.partei_id
    GROUP BY m.land_id, m.partei_id
),

Ueberhangmandate3 AS (
	SELECT dm.land_id, dm.partei_id, dm.sitze - lm.sitze as Anzahl
		FROM DirektmandateProLand dm
		JOIN GewaehlteMandateProLand3 lm 
		ON dm.land_id = lm.land_id
		AND dm.partei_id = lm.partei_id
		WHERE dm.sitze - lm.sitze > 0)

--Nach 2x Ausgleich der Überhangmandate stimmen die Gewaehlten Mandate

SELECT * FROM Ueberhangmandate3