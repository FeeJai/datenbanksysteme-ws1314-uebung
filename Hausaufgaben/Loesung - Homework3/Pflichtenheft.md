#Pflichtenheft

##Zielsetzung
###Musskriterien
####Der Endnutzer

**Über öffentlichen Internetzugang:**

- Kann das System ohne Registierung benutzen
- Kann nach Wahlende das Wahlergebnis anzeigen:
	-  als absolute und prozentuale Zusammenfassung
	-  gewählte Kandidaten
- Kann das Wahlergebnis inkl. aller Details als CSV herunterladen
- Kann lokal gespeicherte Wahlergebnisse importieren (Batch-Loading) und Vergleiche berechnen
- hat weitere Analysemöglichkeiten:
	- Liste mit Überhangmandaten
	- Kandidatenübersicht nach Parteien, Alter, Wohnorten
	- Länderübersicht

**Im Wahllokal:**

- Möglichkeit der Stimmabgabe auf in Wahlkabinen bereitgestellten Maschinen, nach Freigabe durch Wahlhelfer
- Berücksichtigung der Wahlrechtsgrundsätze
	- Allgemeines Wahlrecht
	- Gleiches Wahlrecht
	- Unmittelbarkeit
	- Wahlgeheimnis
	- Wahlfreiheit

####Der Wahlleiter

- legt neue Wahlen an und setzt die Termine fest
- Kann die Bevölkerung der Bundesländer einstellen und somit die Sitzverteilung verändern
- Kann Wahlkreise anlegen und löschen
- trägt die Direktkandidaten, die für die Wahl zugelassenen Parteien, sowie deren Landeslisten ein
- Kann das Wahlgeschehen in Echtzeit verfolgen und die aktuellen Zwischenergebnisse sehen

####Die Wahlhelfer

- erfassen die Stimmzettel während der Stimmauszählung
- schalten die elektronische Stimmabgabe im Wahllokal frei

####Sonstiges

- Speicherung der Stimmzettel als Tupel von Erst- und Zweitstimme
- Aggregation der Wahlergebnisse auf Ebene von Wahlkreisen
- Konformität mit den deutschen Bundeswahlgesetzen
- Aufschlüsselung der Ergebnisse nach Bundesländern


###Sollkriterien

- Aufschlüsselung der Ergebnisse nach Wahlkreisen
- Vergleiche mit den letzten drei Bundestagswahlen
- SOAP Schnittstelle zur Erfassung digitaler Stimmabgaben im Wahllokal

###Kannkriterien

- Analyse und Speicherung der Ergebnisse auf Ebene des Wahllokals

###Abgrenzungskriterien

- Keine Einzelerfassung der Stimmberechtigten, lediglich aggregierter Zahlenwert
- Keine UI zur Stimmabgabe
- Keine Prognosen für den Wahlausgang
- Zwischenergebnisse werden nicht veröffentlicht und lediglich dem Wahlleiter angezeigt
- Keine kommerziellen Zusatzfunktionen, keine Bezahlsysteme
- Keine weiteren Funktionen außerhalb der Stimmabgabe an Hardware im Wahllokal möglich

##Technische Umsetzung

Das Produkt muss zur Gewähleistung größtmöglicher Kompatibilität als Webanwendung konzipiert werden. Die Benutzung darf keine Installation von Software auf Clientseite vorraussetzen. Es muss auf PCs bei einer WXGA-Auflösung und auf einem iPad im Querformat vollständig benutzbar sein.

###Clientseitige Implementierung

Auf Clientseite darf vorrausgesetzt werden:

* Funktionierende Internetanbindung mit IPv4-Stack und DNS; Geschwindigkeit mind. 2 MBit/s
* Betriebssystem:
	* Microsoft Windows 7 oder neuer
	* Apple Mac OS 10.7 oder neuer
* aktueller Webbrowser:
 	- Internet Explorer 10 oder neuer
 	- Google Chrome 26 oder neuer
* JavaScript ist im Webbrowser aktiviert
* keine Installation zusätzlicher Browserplugins außerhalb des Lieferumfangs notwendig
* Funktionsfähigkeit unabhängig von Adobe Flash
 	
###Serverseitige Implementierung

Das Produkt soll auf handelsüblichen vServern mit Linux-Betriebssystem lauffähig sein.

* Datenbanksystem: PostgreSQL
* Webserver: Webrick
* Programmiersprache: Ruby 2.0
* Web-Framework: Ruby on Rails Version 4.0 mit Gems (best practice)
* Javascript-Libraries:
	* JQuery
	* JQuery UI
	* JS CHARTS oder JQPLOT
* CSS-Framework: InK Interface Kit (oder Twitter Bootstrap)

###Sonstiges
Implementierung von Analyse und Berechnungen in der Datenbank, Verwendung des Webframeworks lediglich zur Anzeige