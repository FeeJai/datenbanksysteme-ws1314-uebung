##Vorraussetzungen: 
- Ruby on Rails
- Postgresql

##Ordnerstruktur:
- Benchmark Client: enthält Code für den Benchmark Client inklusive einiger Messungen
- Wahlinfo: enthält die Web Applikation. 
- Daten: Enthält die zu importierenden Daten als CSV-files.
- Hausaufgaben: enthält schriftliche Abgaben (inklusive Queries)

##Setup:
- In database.yml Nutzername und Passwort anpassen.
- Zum Ausführen: rake db:migrate
- Setup-DB.py Skript ausführen um die Daten in die Datenbank einzuspeisen. 
- Starten mittels rails server